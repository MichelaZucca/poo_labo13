/* 
 * File   : Matrice.cpp
 * Author : Mathieu Monteverde, Michela Zucca
 *
 * Date   : 16.03.2017
 * 
 * Implémentation de la classe Matrice qui permet de gérer des matrices 
 * de booléens aléatoires.
 */

#include <stdlib.h>
#include <iostream>
#include "Matrice.h"
#include <time.h>

bool Matrice::randomSeed = false;
using namespace std;

Matrice::Matrice(const int taille) : taille(taille) {
   init();

   if (!randomSeed) {
      srand( (unsigned)time(0) );
      randomSeed = true;
   }

   for (int i = 0; i < taille; i++)
      for (int j = 0; j < taille; j++)
         matrice[i][j] = rand() / (RAND_MAX + 1.0) > 0.5;
}

Matrice::Matrice(const Matrice & A) : taille(A.taille) {
   init();

   for (int i = 0; i < taille; i++)
      for (int j = 0; j < taille; j++)
         matrice[i][j] = A.matrice[i][j];
}

void Matrice::init() {
   if (taille < 0)
      taille = 0;
   matrice = new bool*[taille];
   for (int i = 0; i < taille; i++)
      matrice[i] = new bool[taille];
}

Matrice::~Matrice() {
   for (int i = 0; i < taille; i++)
      delete[] this->matrice[i];

   delete[] this->matrice;
}

void Matrice::opLogique(const Matrice& b, const OperationLogique& op) {
   if (this->taille == b.taille) {
      for (int i = 0; i < taille; ++i) {
         for (int j = 0; j < taille; ++j) {
            matrice[i][j] = op.logique(matrice[i][j], b.matrice[i][j]);
         }
      }
   }
}

Matrice Matrice::opLogiqueCpy(const Matrice& b, const OperationLogique& op)const {
   if (this->taille != b.taille)
      return Matrice(*this);

   Matrice newMatrice(*this);
   newMatrice.opLogique(b, op);
   return newMatrice;
}

Matrice* Matrice::opLogiquePtr(const Matrice& b, const OperationLogique& op)const {
   if (this->taille != b.taille)
      return NULL;

   Matrice* newMatrice = new Matrice(*this);
   (*newMatrice).opLogique(b, op);
   return newMatrice;
}

void Matrice::afficher()const {
   for (int i = 0; i < taille; i++) {
      for (int j = 0; j < taille; j++)
         cout << matrice[i][j] << " ";
      cout << endl;
   }
}
