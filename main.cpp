/* 
 * File   : main.cpp
 * Author : Mathieu Monteverde, Michela Zucca
 *
 * Date   : 16.03.2017
 * 
 * Programme d'exécution d'opérations logiques avec des matrices de type Matrice.
 * Test également le bon fonctionnement de OperationLogique.
 */

#include <cstdlib>
#include <iostream>
#include <time.h>

#include "Matrice.h"

using namespace std;

/*
 * Affiche les résultat des OperationLogique afin de pouvoir vérifier le bon 
 * fonctionnement des opérations logiques
 */
void testOperationLogique(const OperationLogique& op) {
   cout << "0 0 = 0, result : " << op.logique(false, false) << endl
           << "0 1 = 0, result : " << op.logique(false, true) << endl
           << "1 0 = 0, result : " << op.logique(true, false) << endl
           << "1 1 = 1, result : " << op.logique(true, true) << endl;
}

/*
 * Affiche les résultats des opérations logique entre deux matrices afin de 
 * pouvoir vérifier le bon fonctionnement de la classe Matrice.
 */
void testMatriceOperations(const OperationLogique& op, const char* opName) {
   // Création de deux matrices
   Matrice one(4);
   Matrice two(4);

   // Affichage du contenu des deux matrices
   cout << "one:" << endl;
   one.afficher();
   cout << endl;
   cout << "two:" << endl;
   two.afficher();

   // Opération logique avec retour par pointeur
   cout << endl;
   Matrice* ptrMatrice = one.opLogiquePtr(two, op);
   cout << "ptrMatrice*, apres one " << opName << " two:" << endl;
   ptrMatrice->afficher();
   delete ptrMatrice;

   // Opération logique avec retour par valeur
   cout << endl;
   Matrice cpyMatrice = one.opLogiqueCpy(two, op);
   cout << "newMatrice, apres one " << opName << " two:" << endl;
   cpyMatrice.afficher();

   // Opération logique sur la matrice de base
   cout << endl;
   one.opLogique(two, op);
   cout << "one, apres one " << opName << " two:" << endl;
   one.afficher();
}

int main() {
   AND opAnd;
   OR opOr;
   XOR opXor;

   // Test de vérification du bon fonctionnement des opérateurs logiques
   cout << "Test AND" << endl;
   testOperationLogique(opAnd);
   cout << "Test OR" << endl;
   testOperationLogique(opOr);
   cout << "Test XOR" << endl;
   testOperationLogique(opXor);
   cout << endl << endl;

   // Test des opérations sur les matrices
   cout << "Opérateur AND:" << endl;
   testMatriceOperations(opAnd, "and");
   cout << "Opérateur OR:" << endl;
   testMatriceOperations(opOr, "or");
   cout << "Opérateur XOR:" << endl;
   testMatriceOperations(opXor, "xor");
}

