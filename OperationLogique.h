/* 
 * File   : OperationLogique.h
 * Author : Mathieu Monteverde, Michela Zucca
 *
 * Date   : 16.03.2017
 * 
 * Déclaration de la classe Matrice.
 * Classe permettant d'effectuer des opérations logiques entre 2 booléens. 
 * Ce fichier contient la classe abstraites OperationLogique et ses sous classes 
 * respectives.
 */

#ifndef OPERATIONLOGIQUE_H
#define OPERATIONLOGIQUE_H

/*
 *  Classe de base
 */
class OperationLogique {
public:
    /*
     * Méthode abstraite retournant le résultat d'une opération logique entre 
     * les paramètres a et b qui doit être redéfinie dans les sous-classes.
     */
    virtual bool logique(bool a, bool b)const = 0;
};

/*
 * Opération AND logique
 */
class AND : public OperationLogique {
public:
   bool logique(bool a, bool b)const;

};

/*
 * Opération OR logique
 */
class OR : public OperationLogique {
public:
    bool logique(bool a, bool b)const;
};

/*
 * Opération XOR logique
 */
class XOR : public OperationLogique {
public:
     bool logique(bool a, bool b)const;
};

#endif /* OPERATIONLOGIQUE_H */

