/* 
 * File   : OperationLogique.cpp
 * Author : Mathieu Monteverde, Michela Zucca
 *
 * Date   : 16.03.2017
 * 
 * Implémentation de la classe Matrice.
 * Classe permettant d'effectuer des opérations logiques entre 2 booléens. 
 * Ce fichier contient la classe abstraites OperationLogique et ses sous classes 
 * respectives.
 */

#include "OperationLogique.h"

/*
 * Opération AND logique
 */
bool AND::logique(bool a, bool b) const{
   return a & b;
}

/*
 * Opération OR logique
 */
bool OR::logique(bool a, bool b) const{
   return a | b;
}

/*
 * Opération XOR logique
 */
bool XOR::logique(bool a, bool b) const{
   return a ^ b;
}
