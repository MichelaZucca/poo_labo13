/* 
 * File   : Matrice.h
 * Author : Mathieu Monteverde, Michela Zucca
 *
 * Date   : 16.03.2017
 * 
 * Définition de la classe Matrice qui permet de gérer des matrices 
 * de booléens aléatoires.
 * 
 * Remarques: Si la taille souhaitée dans le constructeur est inférieure à 0, 
 * nous avons choisi de construire une matrice de taille 0 par défaut. 
 * Ce choix a été fait car nous ne savions pas si nous avions le droit d'utiliser
 * les exceptions. Nous préférons cela au fait d'utiliser une taille de type
 * non signé dans le constructeur, car le compilateur pourrait ne pas prévenir
 * l'utilisateur s'il passe un paramètre négatif dans ce cas-là.
 */

#ifndef MATRICE_H
#define	MATRICE_H

#include "OperationLogique.h"

class Matrice {
public:
   /*
    * Constructeur: Construit une matrice carrée de taille taille. Si taille < 0,
    * il construit une matrice de taille 0 (cf. Remarques).
    * Param taille, taille de la matrice carré
    */
   Matrice(const int taille);
   /*
    * Constructeur de copie
    * Param A, matrice à copier
    */
   Matrice(const Matrice& A);
   /*
    * Destructeur
    */
   ~Matrice();

   /* 
    * Applique une opération logique entre cette matrice et une autre. Modifie 
    * la matrice qui appelle la méthode.
    * Param b, seconde matrice
    * Param op, opérateur logique à appliquer
    */
   void opLogique(const Matrice& b, const OperationLogique& op);
   /* 
    * Applique une opération logique entre cette matrice et une autre. Retourne 
    * une nouvelle matrice par copie.
    * Param b, seconde matrice
    * Param op, opérateur logique à appliquer
    */
   Matrice opLogiqueCpy(const Matrice& b, const OperationLogique& op)const;
  /* 
   * Applique une opération logique entre cette matrice et une autre. Retourne
   * une nouvelle matrice par pointeur.
   * Param b, seconde matrice
   * Param op, opérateur logique à appliquer
   */
   Matrice* opLogiquePtr(const Matrice&, const OperationLogique& op)const;
   /*
    * Affiche la matrice.
    */
   void afficher()const;

private:
   void init(); // Initialisation des tableaux 

   static bool randomSeed;
   bool** matrice; // Pointeur sur la matrice
   int taille;
};
#endif	/* MATRICE_H */